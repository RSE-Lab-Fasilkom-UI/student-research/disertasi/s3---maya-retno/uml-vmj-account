# UML-DOP diagram - Account Case Study
The UML-DOP diagram is a UML diagram that is modeled with UML-DOP profile,
see the resource of UML-DOP profile [here](https://gitlab.com/RSE-Lab-Fasilkom-UI/prices-2/model-transformation/uml-dop-profile).

Account in the case study has various types, such as:
- **Basic** Account
- Account with **Overdraft**
- Account with **Interest**
- **InterestEstimation** for Account with Interest 
- **CreditWorthiness** for Account
- Account with **DailyLimit**


## Feature Modeling
Variation in Account case study is also modeled with feature diagram. 
The following figure shows the feature diagram of Account case study.
![](account-fd.png)

## UML modeling
Following delta-oriented programming, each variation in Account is realized as a delta module. <br />
In the UML-DOP diagram, a delta is represented as a UML package with stereotype << delta >>. <br />
The UML-DOP is created in Eclipse Modeling Tools, with Papyrus plugin. <br />
Please use Eclipse with Papyrus plugin to view and edit the UML-DOP diagram. <br />

## References
- Download [Eclipse Modeling Tools 2020-12](https://www.eclipse.org/downloads/packages/release/2020-12/r/eclipse-modeling-tools)
- Install [Papyrus](https://www.eclipse.org/papyrus/index.php) Eclipse Plugin v 2020-12 via this [update-site](https://download.eclipse.org/modeling/mdt/papyrus/updates/releases/2020-12)
- UML-DOP profile [repository](https://gitlab.com/RSE-Lab-Fasilkom-UI/prices-2/model-transformation/uml-dop-profile)

